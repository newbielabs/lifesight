var lifesightApp = new lifesightContactForm();

$(function() {
    $('#lifesight-form-contact').submit(function(e) {
        e.preventDefault();

        lifesightApp.submitData(this);
    });
});

function lifesightContactForm()
{
    this.submitData = function(form)
    {
        var ajax = $.ajax({
            url: $(form).attr('action'),
            type: 'post',
            dataType: 'json',
            data: $(form).serialize(),
            beforeSend: function() {
                $('#thxModal').modal('hide');
                $(form).find('.in-error').removeClass('in-error');
                $(form).find('.text-val').remove();
                $(form).find('.fa-times-circle').remove();
            },
            success : function(response) {
                if (response.code == 200)
                {
                    $('#lifesight-form-contact')[0].reset();
                    $('#contactModal').modal('hide');
                    $('#thxModal #notif-msg-ico').prop('src', $('#thxModal .modal-thx').attr('data-img-success'));
                    $('#thxModal .modal-header span').html('Inquiry Sent!');
                    $('#thxModal .thx-1').html('Thank you for your enquiry.');
                    $('#thxModal .thx-2').html('Our representative will be in touch as soon as possible.');
                    $('#thxModal').modal('show');
                }
                else if(response.code == 400)
                {
                    if (lifesightApp.checkNested(response, 'data', 'error', 'items'))
                    {
                        $.each(response.data.error.items, function(key, value) {
                            $(form).find('#' + key).addClass('in-error');
                            $(form).find('#' + key).prepend('<i class="fa fa-times-circle"></i>');
                            $(form).find('#' + key).append('<div class="text-val">'+value+'</div>');
                        });
                    }
                }
                else
                {
                    $('#contactModal').modal('hide');
                    $('#thxModal #notif-msg-ico').prop('src', $('#thxModal .modal-thx').attr('data-img-failed'));
                    $('#thxModal .modal-header span').html('Inquiry Not Sent!');
                    $('#thxModal .thx-1').html('OOPS !');
                    $('#thxModal .thx-2').html('We\'re having a technical difficulties. Please try again later.');
                    $('#thxModal').modal('show');
                }
            },
            error: function(x, t, m) {
                $('#contactModal').modal('hide');
                $('#thxModal #notif-msg-ico').prop('src', $('#thxModal .modal-thx').attr('data-img-failed'));
                $('#thxModal .modal-header span').html('Inquiry Not Sent!');
                $('#thxModal .thx-1').html('OOPS !');
                $('#thxModal .thx-2').html('We\'re having a technical difficulties. Please try again later.');
                $('#thxModal').modal('show');
            }
        });

        ajax = null; delete ajax;
    }

    this.checkNested = function(obj) {
        for (var i = 1; i < arguments.length; i++) 
        {
            if ( ! obj.hasOwnProperty(arguments[i]))
            {
                return false;
            }

            obj = obj[arguments[i]];
        }

        return true;
    }
}
