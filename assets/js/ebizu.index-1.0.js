////
//EBIZU
//URL: ?
//AUTHOR: Nabil Amer Thabit (nbilz//lab//dsign | @nbilz)
//EMAIL: nbilz@live.com
//CREATE DATE: Jan 10, 2018
//UPDATE DATE: Jan 10, 2018
//REVISION: 1
//NAME: ebizu.index-1.0.js
//TYPE: JavaScript
//DESCRIPTION: EBIZU | Campaign
////

$(window).on('load', function(){
    var valDone = $('.the-content > ul > li > .the-section > ul > li > a > span.new').length,
        valAll = $('.the-content > ul > li > .the-section > ul > li > a').length,
        valProg = (valDone / valAll) * 100;
    
    $('.val-done').text(valDone);
    $('.val-all').text(valAll);
    $('.val-remain').text(valAll - valDone);
    $('.the-width').text(valProg.toFixed(2) + '%');
    $('.the-width').delay(500).animate({width: valProg + '%'}, 2000);
    
    $('.the-content > ul').masonry({
        itemSelector: '.the-content > ul > li',
        percentPosition: true,
        transitionDuration: '0.2s'
    });
});