<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common Helper
 * Helper merupakan fungsi tambahan dalam aplikasi
 * @author Asep Fajar Nugraha <delve_brain@hotmail.com>
 */

// FORM VALIDATION
if ( ! function_exists('set_rules'))
{
    function set_rules($rules = array(), $prefix = '', $suffix = '')
    {
        $CI = & get_instance();

        $CI->form_validation->set_error_delimiters($prefix, $suffix);

        foreach ($rules as $rule)
        {
            if ( ! empty($rule[0]))
            {
                $CI->form_validation->set_rules($rule[0], ((!empty($rule[2])) ? $rule[2] : ''), ((!empty($rule[1])) ? $rule[1] : ''));
            }
        }

        unset($CI);
    }
}

if ( ! function_exists('get_rules_error'))
{
    function get_rules_error($rules = array())
    {
        $CI = & get_instance();

        $errors['message']  = validation_errors();
        $errors['items']    = array();
        foreach ($rules as $rule)
        {
            if (( ! empty($rule[0])) && form_error($rule[0]))
            {
                $errors['items'][$rule[0]] = form_error($rule[0]);
            }
        }

        return array('error' => $errors);
    }
}

// URL
if ( ! function_exists('create_query_url'))
{
    function create_query_url($url, $params = array())
    {
        $CI = & get_instance();

        $query = parse_url( $url, PHP_URL_QUERY );
        parse_str( $query, $current_params );

        // merging
        $params = array_replace_recursive($current_params, $params);

        foreach ($params as $key => $value)
        {
            if (empty($value))
            {
                unset($params[$key]);
            }
        }

        // build query string
        $query = http_build_query( $params );

        // build url
        return explode( '?', $url )[0] . '?' . $query;
    }
}
