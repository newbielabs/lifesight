<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Controller
 * @author Asep Fajar Nugraha <delve_brain@hotmail.com>
 */
class Base_Controller extends CI_Controller {
    private $request;
    private $response;
    private $document = array();

    protected $ajax = FALSE;
    protected $data = array();
    protected $language = array();

	public function __construct()
    {
        parent::__construct();

        // INIT Application
        $this->init_application();
    }

    protected function init_application()
    {
        // Check Request Method
        if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
           $this->ajax = TRUE;
        }

        // Language
        $idiom = 'english';
        $this->lang->load(array('common', 'response', 'calendar'), $idiom);
        $this->language = $this->lang->language;

        $this->set_document('app_layout', array('header', 'footer', 'content'));

        $this->data['app_url']      = $this->config->item('base_url');
        $this->data['assets_url']   = $this->config->item('assets_url');
    }

    protected function get_request($key = '', $method = 'POST')
    {
        $method = strtoupper($method);

        if (in_array($method, array('DELETE', 'GET', 'POST', 'PUT')))
        {
            $this->request['HEADER'] = $this->input->request_headers();

            switch($method)
            {
                case 'GET':
                    $this->request['GET'] = $this->input->get();
                break;
                case 'POST':
                    $this->request['POST'] = $this->input->post();
                break;
                case 'PUT':
                    parse_str(file_get_contents("php://input"), $this->request['PUT']);
                break;
                case 'DELETE':
                    parse_str(file_get_contents("php://input"), $this->request['DELETE']);
                break;
            }

            if (isset($this->request[$method]))
            {
                if ( ! empty($key))
                {
                    return (isset($this->request[$method][$key]) ? $this->request[$method][$key] : FALSE);
                }
                else
                {
                    return $this->request[$method];
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    protected function set_response($key = '', $value = '')
    {
        if ( ! empty($key))
        {
            if ($key == 'message')
            {
                $this->response['message'] = trim(preg_replace('/\s\s+/', ' ', $value));
            }
            else
            {
                $this->response[$key] = $value;

                if ($key == 'code')
                {
                    if ( ! empty($this->response['result'])) unset($this->response['result']);
                    $this->response['message'] = sprintf($this->language['error_response'], $this->language['response'][$value]['title'], $this->language['response'][$value]['description']);
                }
            }
        }
        else
        {
            $this->response = $value;
        }
    }

    protected function get_response($key = '')
    {
        if ( ! empty($key))
        {
            if (isset($this->response[$key]))
            {
                return $this->response[$key];
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return $this->response;
        }
    }

    protected function set_session($key = '', $value = '')
    {
        if ( ! empty($key))
            $this->session->set_userdata($key . '-' . $this->config->item('encryption_key'), $value);
    }

    protected function get_session($key = '')
    {
        if ( ! empty($key))
        {
            return $this->session->userdata($key . '-' . $this->config->item('encryption_key'));
        }
        else
        {
            return $this->session->userdata();
        }
    }

    protected function set_document($key = '', $value = '')
    {
        if ( ! empty($key))
            $this->document[$key] = $value;
    }

    protected function get_document($key = '')
    {
        if ( ! empty($key))
        {
            return $this->document[$key];
        }
        else
        {
            return $this->document;
        }
    }

    protected function print_layout($content_layout = '')
    {
        // Document
        $this->data['document'] = $this->get_document();

        $template = array();
        foreach ($this->data['document']['app_layout'] as $key => $layout)
        {
            $template[$layout] = $this->load->view(
                    (($layout === 'content') ? $content_layout : 'layout/' . $layout)
                    , $this->data
                    , true);
        }

        // PRINT
        $this->load->view('layout/container', $template);
    }

    protected function print_output($data = array(), $output = 'json', $check_ajax = TRUE, $exit = TRUE)
    {
        $response_type = ( ! empty($output) ? $output : 'json');
        $response_data = ( ! empty($data) ? $data : $this->get_response());

        if ($check_ajax && ( ! $this->ajax))
        {
            // SET flashdata
            if ($this->get_response('code') !== 200)
                $this->session->set_flashdata('error', $this->get_response('message'));

            // Check if has redirect
            if ($this->get_response('redirect'))
                redirect($this->get_response('redirect'));
        }

        switch ($response_type)
        {
            default:
            case 'json':
                $response = json_encode($response_data);
                $this->output
                    ->set_content_type('application/json', $this->config->item('charset'))
                    ->set_output($response)
                    ->_display();

                if ($exit) exit;
            break;

            case 'xml':
                $xml_data = new SimpleXMLElement('<?xml version="1.0"?><response></response>');
                $this->array_to_xml($response_data, $xml_data);

                $response = $xml_data->asXML();

                header('Content-type: text/xml');
                echo $response;

                if ($exit) exit;
            break;

            case 'plain':
                $response = json_encode($response_data);
                return json_decode($response, 1);
                
                if ($exit) exit;
            break;
        }
    }
}
