<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Base_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		// Load View
		$this->set_document('page_class', 'vid');
        $this->print_layout('home');
	}
}
