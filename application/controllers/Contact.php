<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends Base_Controller {

	public function __construct()
    {
        parent::__construct();
    }

    public function submit()
    {
        $this->set_response('redirect', $this->data['app_url']);

        $this->load->library(array('form_validation'));

        $rules[] = array('first_name', 'trim|required|max_length[50]', 'First Name');
        $rules[] = array('last_name', 'trim|required|max_length[50]', 'Last Name');
        $rules[] = array('email', 'trim|required|max_length[50]|valid_email', 'Email');
        $rules[] = array('company', 'trim|max_length[50]', 'Company');
        $rules[] = array('mobile', 'trim|required|numeric|max_length[15]', 'Mobile');
        $rules[] = array('website', 'trim|max_length[100]', 'Website');
        $rules[] = array('country', 'trim|max_length[100]', 'Country');
        $rules[] = array('industry', 'trim|max_length[100]', 'Industry');
        $rules[] = array('lead_source', 'trim|max_length[100]', 'Lead Source');
        $rules[] = array('message', 'trim', 'Message');

        set_rules($rules);

        if (($this->form_validation->run() == TRUE))
        {
            // Email
            $email_list = file_get_contents(FCPATH . 'email.json');
            $email_list = json_decode($email_list, 1);

            if ( ! empty($email_list['sender']) &&  ! empty($email_list['receiver']))
            {
                $email_list['receiver'] = ( ! is_array($email_list['receiver']) ? array($email_list['receiver']) : $email_list['receiver']);

                // Load Library
                $this->load->library('PHPMailerAutoload');

                $mail = new PHPMailer();

                // CONFIG WITH SMTP
                // $mail->IsSMTP(); 
                // $mail->SMTPAuth   = true;
                // $mail->SMTPSecure = 'tls';
                // $mail->Host       = 'smtp.gmail.com';
                // $mail->Port       = '587';
                // $mail->Username   = 'lifesight@ebizu.com';
                // $mail->Password   = 'ebizu4ebizu';

                try {
                    $mail->SetFrom($email_list['sender'], 'Lifesight Website Enquiry');

                    foreach ($email_list['receiver'] as $key => $value)
                    {
                        $mail->AddAddress($value);
                    }

                    $mail->Subject  = 'New Website Enquiry';
                    $mail->Body     = $this->load->view('email/contact', $this->get_request(), TRUE);
                    $mail->isHTML(true);

                    $mail->Send();

                    $this->set_response('code', 200);
                } catch (phpmailerException $e){
                    $this->set_response('code', 500);
                    $this->set_response('message', $e->getMessage());
                } catch (Exception $e) {
                    $this->set_response('code', 500);
                    $this->set_response('message', $e->getMessage());
                }
            }
            else
            {
                $this->set_response('code', 500);
            }
        }
        else
        {
            $this->set_response('code', 400);
            $this->set_response('message', sprintf($this->language['error_response'], $this->language['response'][400]['title'], trim(preg_replace('/\s+/', ' ', validation_errors()))));
            $this->set_response('data', get_rules_error($rules));
        }

        $this->print_output();
    }
}
