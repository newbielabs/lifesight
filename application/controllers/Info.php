<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends Base_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function insight()
	{
		// Load View
		$this->set_document('page_title', 'Consumer &amp; Location Insights');
        $this->print_layout('insight');
	}

	public function marketing()
	{
		// Load View
		$this->set_document('page_title', 'People Based Marketing');
        $this->print_layout('marketing');
	}

	public function attribution()
	{
		// Load View
		$this->set_document('page_title', 'Omni-channel Attribution');
        $this->print_layout('attribution');
	}

	public function daas()
	{
		// Load View
		$this->set_document('page_title', 'Data-as-a-Service');
        $this->print_layout('daas');
	}
}
