<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends Base_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		// Load View
		$this->set_document('css', array(
			$this->data['assets_url'] . 'css/ebizu.company-1.0.css?v=1',
		));

        $this->print_layout('company');
	}
}
