<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resources extends Base_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		// Load View
		$this->set_document('page_title', 'Resources');
        $this->print_layout('resources');
	}
}
