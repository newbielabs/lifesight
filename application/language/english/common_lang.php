<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['lang_code']      = 'en';
$lang['lang_locale']    = 'en_US';
$lang['lang_name']      = 'English';

// Numeric
$lang['decimal_symbol']     = ',';
$lang['thousands_symbol']   = '.';

$lang['error_message'] 		= '(%s) %s';
$lang['error_response'] 	= '%s - %s';
$lang['error_common'] 		= 'Something Error.';
$lang['error_parameter'] 	= 'Parameter not valid.';
$lang['error_not_found'] 	= 'Data not found.';
$lang['error_password'] 	= 'The password you entered is incorrect.';

$lang['success_save'] 		= 'Data succesfully saved.';
$lang['success_remove'] 	= 'Data succesfully removed.';

$lang['message_data_empty'] = 'No data available or empty.';
