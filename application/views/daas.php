<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Header -->
<header class="masthead" id="dass" style="padding-bottom: 135px;">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Access &amp; Consume Our Propietary Data On Your Own Terms</div>
			<div class="intro-lead-in">Enrich your own customer knowledge like never before with our proprietary data sets &amp; gain an edge over your competitors today</div>
			<a class="btn btn-xl js-scroll-trigger btn-act" id="playme" onclick="revealVideo('video','youtube')">Play The Video</a>
			<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal">Let's Talk</a>
		</div>        
	</div>
</header>
<div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
	<div class="lightbox-container">  
		<div class="lightbox-content">
			<button onclick="hideVideo('video','youtube')" class="lightbox-close">
				Close | ✕
			</button>
			<div class="video-container">
				<iframe id="youtube" width="960" height="520" src="https://www.youtube.com/embed/rTGom-T9S3E?showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>      

		</div>
	</div>
</div>

<!-- Lifesight Data -->
<section id="lifesightdata" class="five-element">
	<div class="container">
		<div class="row">
			<div class="row container main-data">
				<div class="part">
					<div class="infographic">
						<div class="title">
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number">100</div> <span>%</span>
						</div>
						<div class="content">
							SDK based Location Data
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number">220</div> <span>m</span>
						</div>
						<div class="content">
							SDK based Location Data
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<span>$</span><div class="number">1</div><span>B</span>
						</div>
						<div class="content">
							SDK based Location Data                  
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number slow">500</div><span>+</span>
						</div>
						<div class="content">
							SDK based Location Data               
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Feature -->
<section id="feature">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Features</h2>
				<div class="borderheading"></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="row container main-feature">
				<div class="col-md-6 part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/features/feature-one.png">
						<div class="col-md-8 text">
							<div class="title">
								Enrich Your Data
							</div>
							<div class="border"></div>
							<div class="info">
								You know who some of your customers are. However this data may not be usable or useful to you to take an action or make a decision. Enriching your data with ours could reveal powerful analytics.
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6 part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/features/feature-two.png">
						<div class="col-md-8 text">
							<div class="title">
								Activate Your Audience
							</div>
							<div class="border"></div>
							<div class="info">
								Our 100% SDK based audience gives us the ability to reach your audience via any mobile ad network. Simply use a common identifier to retrieve their mobile advertiser ID
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6 part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/features/feature-three.png">
						<div class="col-md-8 text">
							<div class="title">
								Media Attribution
							</div>
							<div class="border"></div>
							<div class="info">
								Use our accurate location and transactional data to attribute in-store visits and sales to media campaigns such as TV, Radio, Print and OOH.
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-6 part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/features/feature-four.png">
						<div class="col-md-8 text">
							<div class="title">
								API or Bulk Transfer
							</div>
							<div class="border"></div>
							<div class="info">
								Whatever data platform you use, you can be assured that our data will be usable. We support direct real time API data transfer or periodical bulk data transfer. 
							</div>
						</div>
					</div> 
				</div>
				<a data-toggle="modal" data-target="#contactModal"><button class="btn btn-act">
					Get Our Data
				</button></a>
			</div>
		</div>
	</div>
</section>

<!-- Quotes DaaS -->
<section id="quotes-middle">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Our Location data is sourced from 100% SDK data collected from a range of 3rd party app publishers</h2>
			</div>
		</div>
	</div>
</section>

<!-- Use Case -->
<section id="usecase" class="trio">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Use Cases</h2>
				<div class="borderheading"></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="row container main-feature">
				<div class="col-md-4 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/usecase/usecase-1.png">
						<div class="col-md-12 text">
							<div class="title">
								Consumer Data Enrichment
							</div>
							<div class="border"></div>
							<div class="info">
								If you own a data platform or deal with market research, you would like to enrich your data sets with other valuable consumer data sources. Our unique location and transaction data sets can enhance your view of your customer like you never seen before.
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-4 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/usecase/usecase-2.png">
						<div class="col-md-12 text">
							<div class="title">
								Omnichannel Distribution
							</div>
							<div class="border"></div>
							<div class="info">
								If your are an agency or media company, you know that CTRs and Ad Recall Rate KPIs just don't cut it anymore. Brands are demanding to know whether their spend is driving footfall and real purchases, so use our data today to enable this.
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-4 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/usecase/usecase-3.png">
						<div class="col-md-12 text">
							<div class="title">
								Location Insight
							</div>
							<div class="border"></div>
							<div class="info">
								If you are a government or enterprise, you would like to further enrich your data with more people movement data to help you understand trends and make better decisions. Integrate our DaaS platform and source high quality location analytics today.
							</div>
						</div>
					</div> 
				</div>
				<a data-toggle="modal" data-target="#contactModal"><button class="btn btn-act">
					Charge Up Your Data Here
				</button></a>
			</div>
		</div>
	</div>
</section>

<!-- Industry Solutions -->
<section id="industrysolution" class="fourparts">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Industry Solutions For</h2>
				<div class="borderheading"></div>
			</div>
		</div>
		<div class="row container main-data">
	        <div class="part">
	        	<div class="infographic">
	        		<img src="<?php echo $assets_url; ?>img/industry-solutions/ind-sol-brands.png">
	        		<div class="title">
	        			Brands
	        		</div>
	        	</div>  
	        </div>
	        <div class="part">
	        	<div class="infographic">
	        		<img src="<?php echo $assets_url; ?>img/industry-solutions/ind-sol-medcom.png">
	        		<div class="title">
	        			Media Companies
	        		</div>
	        	</div>  
	        </div>
	        <div class="part">
	        	<div class="infographic">
	        		<img src="<?php echo $assets_url; ?>img/industry-solutions/ind-sol-dsps.png">
	        		<div class="title">
	        			DSPs
	        		</div>
	        	</div> 
	        </div>
	        <div class="part dmps">
	        	<div class="infographic">
	        		<img src="<?php echo $assets_url; ?>img/industry-solutions/ind-sol-dmps.png">
	        		<div class="title">
	        			DMPs
	        		</div>
	        	</div>  
	        </div>
	    </div>
	</div>
</section>