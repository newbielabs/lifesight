<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Header -->
<header class="masthead" id="marketing">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Target People, Not Pixels with Our Proprietary Audience Reach</div>
			<div class="intro-lead-in">Publish highly accurate advertising campaigns with our high performance DSP and DMP solution that contains proprietary audience segments generated through deterministic data segments</div>
			<a class="btn btn-xl js-scroll-trigger btn-act" id="playme" onclick="revealVideo('video','youtube')">Play The Video</a>
			<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal">Let's Talk</a>
		</div>        
	</div>
</header>

<div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
	<div class="lightbox-container">  
		<div class="lightbox-content">
			<button onclick="hideVideo('video','youtube')" class="lightbox-close">
				Close | ✕
			</button>
			<div class="video-container">
				<iframe id="youtube" width="960" height="520" src="https://www.youtube.com/embed/rTGom-T9S3E?showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>      

		</div>
	</div>
</div>

<!-- Lifesight Data -->
<section id="lifesightdata" class="five-element">
	<div class="container">
		<div class="row">
			<div class="row container main-data">
				<div class="part">
					<div class="infographic">
						<div class="title">

						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number">50</div> <span>M+</span>
						</div>
						<div class="content">
							Active User Profiles
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number">300</div> <span>k+</span>
						</div>
						<div class="content">
							Location Tracked
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number">250</div><span>m</span>
						</div>
						<div class="content">
							Txns Recorded                
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number slow">200</div><span>+</span>
						</div>
						<div class="content">
							Segments              
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Feature -->
<section id="feature" class="two-col">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Features</h2>
				<div class="borderheading"></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="row col-md-6 main-feature">
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/marketing/feature-mark-1.png">
						<div class="col-md-8 text">
							<div class="title">
								Realtime Location Targeting
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/marketing/feature-mark-2.png">
						<div class="col-md-8 text">
							<div class="title">
								Accurate Segments
							</div>
							<div class="border"></div>
							<div class="info">
								We offer anonymous consumer profile insights which enables a view of your location visitors that are on our platform.
							</div>
						</div>
					</div> 
				</div>
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/marketing/feature-mark-3.png">
						<div class="col-md-8 text">
							<div class="title">
								Innovative Ad Formats
							</div>
							<div class="border"></div>
							<div class="info">
								We collect and process transactional data to provide a sample of spend behaviour across thousands of retailers in Southeast Asia.
							</div>
						</div>
					</div> 
				</div>
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/marketing/feature-mark-4.png">
						<div class="col-md-8 text">
							<div class="title">
								Advanced Attribution
							</div>
							<div class="border"></div>
							<div class="info">
								Analyze the performance of your media campaigns by viewing the change in your location visit data during a certain period.
							</div>
						</div>
					</div> 
				</div>
				<a data-toggle="modal" data-target="#contactModal"><button class="btn btn-act">
					Launch Your Campaign
				</button></a>
			</div>
			<div class="row col-md-6 screen">
				<img src="<?php echo $assets_url; ?>img/marketing-section3.png" width="570" height="542">
			</div>
		</div>
	</div>
</section>

<!-- Industry Solutions -->
<section id="industrysolutions">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center header">
				<h2 class="section-heading">Industry solutions for</h2>
				<div class="borderheading"></div>
			</div>
			<ul>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-fmcg.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/fmcg.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					FMCG & CPG
				</li>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-car.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/automotive.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					Automotive
				</li>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-money.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/banking.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					Banking & Finance
				</li>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-retail.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/retail.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					Retail & QSR
				</li>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-telco.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/telco.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					Telco
				</li>
			</ul>
		</div>
	</div>
</section>

<!-- Place Sense -->
<section id="place-sense">
	<div class="container">
		<div class="row tex-center">
			<div class="col-md-6 text">
				<img src="<?php echo $assets_url; ?>img/PlaceSense.png">
				<div class="title">
					Micro Location Insights & Intelligence
				</div>
				<div class="border"></div>
				<span>Introducing the most deterministic micro location intelligence platform in Asia that collects GPS, WiFi & Beacons data via a network of premium app publishers to accurately drive people movement insights use cases.</span>
			</div>
			<div class="col-md-6 display">
				<img height="540" src="<?php echo $assets_url; ?>img/video/Maps_Web_Blue_NV.gif">
			</div>
		</div>
	</div>
</section>

<!-- How It Works -->
<section id="usecase" class="trio markethow">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center header">
				<h2 class="section-heading">How It Works</h2>
				<div class="borderheading"></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-md-4 part">
				<div class="row insight">
					<img src="<?php echo $assets_url; ?>img/marketing/map-1.png" width="266" height="266">
					<div class="col-md-12 text">
						<div class="title">
							Mapping Data
						</div>
						<div class="border"></div>
						<div class="info readmore">
							Before location data can be turned into intelligence, it’s critical to have a map. And not just any map — a highly precise map of physical locations in the real world. We are data purists at heart, which is why we carefully source routing data, event data, demographic data and more. To amplify this data, we also employ cartographers to hand draw the physical outlines of points-of-interest — including retail stores, restaurants, parking lots and more.
						</div>
					</div>
				</div> 
			</div>
			<div class="col-md-4 part">
				<div class="row insight">
					<img src="<?php echo $assets_url; ?>img/marketing/map-2.png" width="266" height="266">
					<div class="col-md-12 text">
						<div class="title">
							Location Data
						</div>
						<div class="border"></div>
						<div class="info readmore">
							Mobile devices constantly emit location signals through the form of latitude/longitude coordinates. We source massive amounts of anonymous device signals and connect them with our basemap. This helps us understand where audiences are moving in the physical world
						</div>
					</div>
				</div> 
			</div>
			<div class="col-md-4 part">
				<div class="row insight">
					<img src="<?php echo $assets_url; ?>img/marketing/map-3.png" width="266" height="266">
					<div class="col-md-12 text">
						<div class="title">
							Diversity of Data
						</div>
						<div class="border"></div>
						<div class="info readmore">
							To deliver scalable, accurate solutions, we’ve made it our mission to diversify our data. That's why we work with thousands of premier data sources to combine the rich depth and accuracy of background data with the wide coverage of foreground data. This intricate level of data diversity is critical for richly detailed solutions.
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>
</section>

<!-- Customer Identification -->
<section id="identification">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">We know your customers, anonymously</h2>
				<h3 class="section-subheading text-muted">We track and profile millions of customers demographics, behaviours, interests and preferences anonymously to enable advertisers to target consumers with highly personalized experiences</h3>
			</div>
		</div>
		<div class="row text-center">
			<div class="row container main-customer">
				<div class="col-md-3 part">
					<div class="insight">
						<div class="box">
							<img class="phopic" src="<?php echo $assets_url; ?>img/customers/john.png">
							<i><img src="<?php echo $assets_url; ?>img/customers/icons/icon1.png"></i>
							<div class="title">
								3449532355
							</div>
							<div class="profile">
								Male, 36, Kuala Lumpur <span>Consultant, High Spend</span>
							</div>
							<div class="define">
								Coffee Lover, Fitness Freak, Movie Buff, Traveller
							</div>
							<div class="seen">
								<ul>
									<li>Seen at coffee outlets</li>
									<li>Seen at malls often</li>
									<li>Seen at baby shops and gyms</li>
									<li>See ads 3 times today</li>
									<li>Apps: CIMB Bank, Expedia, Booking.com, Bloomberg</li>
								</ul>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">

						<div class="box">
							<img class="phopic" src="<?php echo $assets_url; ?>img/customers/adam.png">
							<i><img src="<?php echo $assets_url; ?>img/customers/icons/icon2.png"></i> 
							<div class="title">
								3449532355
							</div>
							<div class="profile">
								Female, 20, Jakarta <span>Student, Low Spend</span>
							</div>
							<div class="define">
								Music Lover, Movie/TV Buff, Gamer, Online Shopper
							</div>
							<div class="seen">
								<ul>
									<li>Seen at restaurants and cafes</li>
									<li>Seen at malls weekly</li>
									<li>Seen on online shopping sites</li>
									<li>See ads 10 times today</li>
									<li>Apps: Lazada, Zalora, Spotify, Amazon, Netflix</li>
								</ul>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">
						<div class="box">
							<img class="phopic" src="<?php echo $assets_url; ?>img/customers/kevi.png">
							<i><img src="<?php echo $assets_url; ?>img/customers/icons/icon3.png"></i>
							<div class="title">
								3449532355
							</div>
							<div class="profile">
								Male, 48, Singapore <span>Banker, High Spend</span>
							</div>
							<div class="define">
								Traveler, Golfer, Fine Dining, Live Entertainment
							</div>
							<div class="seen">
								<ul>
									<li>Seen at restaurants and bars</li>
									<li>Seen at golf courses</li>
									<li>Seen at airports</li>
									<li>See ads 6 times today</li>
									<li>Seen at restaurants and bars</li>
									<li>Seen at golf courses</li>
									<li>Seen at airports</li>
									<li>See ads 6 times today</li>
									<li>Apps: Bloomberg, CNN, GolfBuddy, Singapore Airlines</li>
								</ul>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">
						<div class="box">
							<img class="phopic" src="<?php echo $assets_url; ?>img/customers/vicky.png">
							<i><img src="<?php echo $assets_url; ?>img/customers/icons/icon4.png"></i>
							<div class="title">
								3449532355
							</div>
							<div class="profile">
								Female, 32, Singapore <span>Marketer, High Spend</span>
							</div>
							<div class="define">
								Shopaholic, Coffee Lover, Sweeth tooth, Mom
							</div>
							<div class="seen">
								<ul>
									<li>Seen at hypermarket and mall</li>
									<li>Seen at apparel & shoe shops</li>
									<li>Seen at kids shops</li>
									<li>Seen ads 15 times today</li>
									<li>Apps: MomsCare, Spotify, Netflix, CandyCrush</li>
								</ul>
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>