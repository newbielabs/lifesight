<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Header -->
<header class="masthead" id="attribution" style="padding-bottom: 135px;">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Leverage Our Offline Conversion Data to Determine Your Campaign ROIs</div>
			<div class="intro-lead-in">Let our unique physical world conversion measurement give you an unprecedented view into your ad campaign attribution</div>
			<a class="btn btn-xl js-scroll-trigger btn-act" id="playme" onclick="revealVideo('video','youtube')">Play The Video</a>
			<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal">Let's Talk</a>
		</div>        
	</div>
</header>

<div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
	<div class="lightbox-container">  
		<div class="lightbox-content">
			<button onclick="hideVideo('video','youtube')" class="lightbox-close">
				Close | ✕
			</button>
			<div class="video-container">
				<iframe id="youtube" width="960" height="520" src="https://www.youtube.com/embed/rTGom-T9S3E?showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>      

		</div>
	</div>
</div>

<!-- Quotes DaaS -->
<section id="quotes-dass" class="attr">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<p>It’s about time you answer the questions CMOs around the world have been asking for decades</p>
				<h2 class="section-heading" style="font-size: 24px;">Is my ad spend <span>bringing footfall to my outlet?</span></h2>
			</div>
		</div>
	</div>
</section>

<!-- Feature -->
<section id="feature" class="two-col">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Features</h2>
				<div class="borderheading"></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="row col-md-6 main-feature">
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/insight/location.png">
						<div class="col-md-8 7ext">
							<div class="title">
								Location
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/insight/consumer.png">
						<div class="col-md-8 text">
							<div class="title">
								In-store Transactions
							</div>
							<div class="border"></div>
							<div class="info">
								We offer anonymous consumer profile insights which enables a view of your location visitors that are on our platform.
							</div>
						</div>
					</div> 
				</div>
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/insight/transaction.png" width="182">
						<div class="col-md-8 text">
							<div class="title">
								Transactional
							</div>
							<div class="border"></div>
							<div class="info">
								We collect and process transactional data to provide a sample of spend behaviour across thousands of retailers in Southeast Asia.
							</div>
						</div>
					</div> 
				</div>
				<a data-toggle="modal" data-target="#contactModal"><button class="btn btn-act">
					Get Access To Your Data
				</button></a>
			</div>
			<div class="row col-md-6 screen">
				<img src="<?php echo $assets_url; ?>img/attr-section3.png" width="555" height="579">
			</div>
		</div>
	</div>
</section>

<!-- Industry Solutions -->
<section id="industrysolutions">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center header">
				<h2 class="section-heading">Industry solutions for</h2>
				<div class="borderheading"></div>
			</div>
			<ul>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-fmcg.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/fmcg.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					FMCG &amp; CPG
				</li>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-car.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/automotive.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					Automotive
				</li>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-money.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/banking.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					Banking &amp; Finance
				</li>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-retail.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/retail.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					Retail &amp; QSR
				</li>
				<li>
					<div class="sol-icon">
						<img src="<?php echo $assets_url; ?>img/solution/icon-telco.png" class="<?php echo $assets_url; ?>img-icon" />
						<img src="<?php echo $assets_url; ?>img/solution/telco.jpg" class="<?php echo $assets_url; ?>img-back" />
					</div>
					Telco
				</li>
			</ul>
		</div>
	</div>
</section>

<!-- How It Works -->
<section id="usecase" class="trio how">
	<div class="row atr">
		<div class="text-center col-md-12">
			<h2 class="section-heading">How It Works</h2>
			<div class="borderheading"></div>
		</div>
		<div class="col-md-6">
			<div class="col-lg-12 text-center header">
				<img src="<?php echo $assets_url; ?>img/attribution/your-dig-campaign.png">
				<label>Your Digital Campaign</label>
			</div>
			<img class="line" src="<?php echo $assets_url; ?>img/connect-line.png">
		</div>
		<div class="text-center col-md-6">
			<div class="row container main-feature">
				<div class="col-md-12 part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/attribution/attr-1.png">
						<div class="col-md-6 text">
							<div class="title">
								Location Attribution
							</div>
							<div class="border"></div>
							<div class="info">
								Store visit data captured via GPA, WiFi &amp; Beacons
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-12 part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/attribution/attr-2.png">
						<div class="col-md-6 text">
							<div class="title">
								Transaction Attribution
							</div>
							<div class="border"></div>
							<div class="info">
								Txn data captured via POS, card data &amp; receipt capture
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-12 part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/attribution/attr-3.png">
						<div class="col-md-6 text">
							<div class="title">
								SKU Level Attribution
							</div>
							<div class="border"></div>
							<div class="info">
								Product level data captured via OCR of receipt capture
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>
		<div class="text-center col-md-12">
			<button class="btn btn-act" data-toggle="modal" data-target="#contactModal">Request A Demo</button>
		</div>
	</div>
</section>

<!-- How It Works -->
<section id="usecase" class="trio how beyond">
	<div class="row atr">
		<div class="text-center col-md-12">
			<h2 class="section-heading">Beyond Consumer Reports</h2>
			<div class="borderheading"></div>
			<p style="font-family: 'Proxima Regular'">LifeSight is about all about understanding the holistic consumer journey. Our attribution report provides you additional consumer insights related to your product activity in retail outlets across the country</p>
		</div>
		<div class="text-center col-md-6">
			<div class="row container main-feature">
				<div class="col-md-12 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/features/feature-two.png">
						<div class="col-md-9 text">
							<div class="title">
								Consumer Loyalty &amp; Journey
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-12 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/platform/people-based.png">
						<div class="col-md-9 text">
							<div class="title">
								Product Prices &amp; Promotions 
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-12 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/features/feature-three.png">
						<div class="col-md-9 text">
							<div class="title">
								Market Share &amp; Consumption
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>
		<div class="text-center col-md-6">
			<div class="row container main-feature">
				<div class="col-md-12 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/insight/product.png">
						<div class="col-md-9 text">
							<div class="title">
								Channel Insights
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-12 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/insight/location.png">
						<div class="col-md-9 text">
							<div class="title">
								Basket Adjacencies
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-12 part">
					<div class="row insight">
						<img src="<?php echo $assets_url; ?>img/usecase/usecase-1.png">
						<div class="col-md-9 text">
							<div class="title">
								Brand Value &amp; Growth
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>
		<div class="text-center col-md-12">
			<button class="btn btn-passive" data-toggle="modal" data-target="#contactModal" style="color: #fff;text-transform: uppercase;">Request A Demo</button>
		</div>
	</div>
</section>