<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<ul>
		<li>First Name: <strong><?php echo ( ! empty($first_name) ? $first_name : '-'); ?></strong></li>
		<li>Last Name: <strong><?php echo ( ! empty($last_name) ? $last_name : '-'); ?></strong></li>
		<li>Email: <strong><?php echo ( ! empty($email) ? $email : '-'); ?></strong></li>
		<li>Company: <strong><?php echo ( ! empty($company) ? $company : '-'); ?></strong></li>
		<li>Mobile: <strong><?php echo ( ! empty($mobile) ? $mobile : '-'); ?></strong></li>
		<li>Website: <strong><?php echo ( ! empty($website) ? $website : '-'); ?></strong></li>
		<li>Country: <strong><?php echo ( ! empty($country) ? $country : '-'); ?></strong></li>
		<li>Industry: <strong><?php echo ( ! empty($industry) ? $industry : '-'); ?></strong></li>
		<li>Lead Source: <strong><?php echo ( ! empty($lead_source) ? $lead_source : '-'); ?></strong></li>
		<li>message: <strong><?php echo ( ! empty($message) ? $message : '-'); ?></strong></li>
	</ul>
</body>
</html>