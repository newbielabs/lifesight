<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<header class="masthead" id="resources" style="padding-bottom: 135px;">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Resources</div>
		</div>        
	</div>
</header>
<div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
	<div class="lightbox-container">  
		<div class="lightbox-content">
			<button onclick="hideVideo('video','youtube')" class="lightbox-close">
				Close | ✕
			</button>
			<div class="video-container">
				<iframe id="youtube" width="960" height="520" src="https://www.youtube.com/embed/rTGom-T9S3E?showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>      

		</div>
	</div>
</div>

<!-- Blog Horizontal -->
<section id="blog-hor">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-4">
				<h3>Blog</h3>
				<div class="box">
					<img src="<?php echo $assets_url; ?>img/resources/blog-1.png">
					<h4 class="blog-heading">Location Targeted Ad-Spend To Rise To USD$32 Billion By 2021 Globally</h4>
					<h5 class="blog-author">By EBIZU | <span class="type">Blog</span> | <span class="comment">No Comment</span></h5>
					<p>Nearly half of overall mobile ad-revenue will be generated from Geo-Targeted Ad-Spend within the next five years..</p>
				</div>
				<div class="row bottom">
					<div class="col-md-6 date">
						January 27, 2017
					</div>
					<div class="col-md-6 liked">
						<i class="fa fa-heart-o text-muted" aria-hidden="true"></i> 10
					</div>
				</div>
				<a href="http://resources.lifesight.io/blog/category/blog"><button class="btn btn-act">
					Read More Blog
				</button></a>
			</div>
			<div class="col-md-4">
				<h3>Articles</h3>
				<div class="box">
					<img src="<?php echo $assets_url; ?>img/resources/blog-2.png">
					<h4 class="blog-heading">Location Driven O2O Attribution</h4>
					<h5 class="blog-author">By Logan | <span class="type">Articles</span> | <span class="comment">No Comment</span></h5>
					<p>In a marketing ecosystem that is getting increasingly complex and number-driven, everyone wants to get attribution right. It forms the very core of marketing decision-making today. So the challenge is not of intent. Then why has attribution remained an area that still needs tremendous unlearning and relearning even now?</p>

				</div>
				<div class="row bottom">
					<div class="col-md-6 date">
						December 13, 2017
					</div>
					<div class="col-md-6 liked">
						<i class="fa fa-heart"  text-mutedaria-hidden="true"></i> 10
					</div>
				</div>
				<a href="http://resources.lifesight.io/blog/category/articles/"><button class="btn btn-act">
					Read More Articles
				</button></a>
			</div>
			<div class="col-md-4">
				<h3>In The News</h3>
				<div class="box">
					<img src="<?php echo $assets_url; ?>img/resources/blog-3.png">
					<h4 class="blog-heading">Sales just a swipe away! Digital retail solutions provider, Ebizu, teams up with RHB to offer simplified e-retail solution</h4>
					<h5 class="blog-author">By Nik | <span class="type">In The News</span> | <span class="comment">No Comment</span></h5>
					<p>Digital Retail Solutions provider Ebizu teams up with RHB to offer simplified e-retail solution</p>

				</div>
				<div class="row bottom">
					<div class="col-md-6 date">
						July 24, 2015
					</div>
					<div class="col-md-6 liked">
						<i class="fa fa-heart-o text-muted" aria-hidden="true"></i> 2
					</div>
				</div>
				<a href="http://resources.lifesight.io/blog/category/in-the-news/"><button class="btn btn-act">
					Read More In The News
				</button></a>
			</div>
		</div>
	</section>

	<!-- Quotes -->
	<section id="quotes-flat">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">All the news by date posted</h2>

				</div>
			</div>
		</div>
	</section>

	<!-- Blog Horizontal -->
	<section id="post-ver">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-10 left">
					<h3>Recent Posts</h3>
					<div class="box row">
						<img class="col-md-5" src="<?php echo $assets_url; ?>img/resources/blog-4.png">
						<div class="col-md-7 content">
							<div class="row top">
								<div class="col-md-6 kind">
									Articles
								</div>
								<div class="col-md-6 liked">
									<i class="fa fa-heart" aria-hidden="true"></i> 10
								</div>
							</div>
							<a href="http://resources.lifesight.io/blog/2017/12/13/location-driven-o2o-attribution"><h4 class="blog-heading">Location Driven O2O Attribution</h4></a>
							<h5 class="blog-author">By Logan | <span class="type">Articles</span> | <span class="comment">No Comment</span></h5>
							<p>In a marketing ecosystem that is getting increasingly complex and number-driven, everyone wants to get attribution right. It forms the very core of marketing decision-making today. So the challenge is not of intent. Then why has attribution remained an area that still needs tremendous unlearning and relearning even now?</p>
							<div class="row bottom">
								<div class="col-md-6 date">
									December 13, 2017
								</div>
								<div class="col-md-6 read">8 min read
								</div>
							</div>
						</div>
					</div>
					<div class="box row">
						<img class="col-md-5" src="<?php echo $assets_url; ?>img/resources/blog-5.png">
						<div class="col-md-7 content">
							<div class="row top">
								<div class="col-md-6 kind">
									In The News
								</div>
								<div class="col-md-6 liked">
									<i class="fa fa-heart-o text-muted" aria-hidden="true"></i> 10
								</div>
							</div>
							<a href="http://resources.lifesight.io/blog/2017/12/13/sports-retailers-data-to-the-rescue"><h4 class="blog-heading">Sports Retailers - Are You Still Shooting Arrows In The Dark?</h4></a>
							<h5 class="blog-author">By Logan | <span class="type">In The News</span> | <span class="comment">No Comment</span></h5>
							<p>Sports retailers need a quick and amazing new marketing strategy, which is as data driven as their other retail counterparts. Geomarketing is one way to achieve it, because nothing is more of a give away of a customer’s hobbies and lifestyles as the places he goes when he is not at work!</p>
							<div class="row bottom">
								<div class="col-md-6 date">
									December 13, 2017
								</div>
								<div class="col-md-6 read">8 min read
								</div>
							</div>
						</div>
					</div>
					<div class="box row">
						<img class="col-md-5" src="<?php echo $assets_url; ?>img/resources/blog-6.png">
						<div class="col-md-7 content">
							<div class="row top">
								<div class="col-md-6 kind">
									Articles
								</div>
								<div class="col-md-6 liked">
									<i class="fa fa-heart-o text-muted" aria-hidden="true"></i> 10
								</div>
							</div>
							<a href="http://resources.lifesight.io/blog/2017/12/12/data-driven-real-estate-marketing"><h4 class="blog-heading">Real Estate Marketing - Data To The Rescue</h4></a>
							<h5 class="blog-author">By Logan | <span class="type">Articles</span> | <span class="comment">No Comment</span></h5>
							<p>Data allows real estate companies and middlemen such as dealers and brokers make engaging comparisons – between locations, between features, between neighborhood and safety best practices, as well as the right audience for each property.</p>
							<div class="row bottom">
								<div class="col-md-6 date">
									December 13, 2017
								</div>
								<div class="col-md-6 read">8 min read
								</div>
							</div>
						</div>
					</div>
					<div class="box row">
						<img class="col-md-5" src="<?php echo $assets_url; ?>img/resources/blog-7.png">
						<div class="col-md-7 content">
							<div class="row top">
								<div class="col-md-6 kind">
									Articles
								</div>
								<div class="col-md-6 liked">
									<i class="fa fa-heart-o text-muted" aria-hidden="true"></i> 10
								</div>
							</div>
							<a href="http://resources.lifesight.io/blog/2017/11/25/cmos-really-data-driven"><h4 class="blog-heading">CMO's - Are You Really Data-Driven?</h4></a>
							<h5 class="blog-author">By Logan | <span class="type">Articles</span> | <span class="comment">No Comment</span></h5>
							<p>CMO’s today are required to play the role or somewhat close of the Chief Data Officer. Generic consumer insights gleaned from small samples of focused groups and primary research is hardly the rage anymore – it is a given.</p>
							<div class="row bottom">
								<div class="col-md-6 date">
									November 25, 2017
								</div>
								<div class="col-md-6 read">8 min read
								</div>
							</div>
						</div>
					</div>
					<div class="box row">
						<img class="col-md-5" src="<?php echo $assets_url; ?>img/resources/blog-8.png">
						<div class="col-md-7 content">
							<div class="row top">
								<div class="col-md-6 kind">
									In The News
								</div>
								<div class="col-md-6 liked">
									<i class="fa fa-heart-o text-muted" aria-hidden="true"></i> 10
								</div>
							</div>
							<a href="http://resources.lifesight.io/blog/2017/11/22/fractional-o2o-attribution"><h4 class="blog-heading">Fractional O2O Attribution</h4></a>
							<h5 class="blog-author">By Logan | <span class="type">In The News</span> | <span class="comment">No Comment</span></h5>
							<p>How Fractional O2O Attribution Will Improve Your Campaign Measurement.</p>
							<div class="row bottom">
								<div class="col-md-6 date">
									November 22, 2017
								</div>
								<div class="col-md-6 read">8 min read
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 right">
					<h3>Archives</h3>
					<ul>
						<li><a href="http://resources.lifesight.io/blog/2017/12">December 2017</a></li>
						<li><a href="http://resources.lifesight.io/blog/2017/11">November 2017</a></li>
						<li><a href="http://resources.lifesight.io/blog/2017/05">May 2017</a></li>
						<li><a href="http://resources.lifesight.io/blog/2017/01">January 2017</a></li>
						<li><a href="http://resources.lifesight.io/blog/2016/06">June 2016</a></li>
						<li><a href="http://resources.lifesight.io/blog/2016/05">May 2016</a></li>
						<li><a href="http://resources.lifesight.io/blog/2016/02">February 2016</a></li>
						<li><a href="http://resources.lifesight.io/blog/2016/01">January 2016</a></li>
						<li><a href="http://resources.lifesight.io/blog/2015/08">August 2015</a></li>
						<li><a href="http://resources.lifesight.io/blog/2015/07">July 2015</a></li>
						<li><a href="http://resources.lifesight.io/blog/2015/06">June 2015</a></li>
						<li><a href="http://resources.lifesight.io/blog/2015/05">May 2015</a></li>
						<li><a href="http://resources.lifesight.io/blog/2015/03">March 2015</a></li>
						<li><a href="http://resources.lifesight.io/blog/2014/12">December 2014</a></li>
						<li><a href="http://resources.lifesight.io/blog/2014/08">August 2014</a></li>
						<li><a href="http://resources.lifesight.io/blog/2014/07">July 2014</a></li>
						<li><a href="http://resources.lifesight.io/blog/2014/06">June 2014</a></li>
						<li><a href="http://resources.lifesight.io/blog/2014/05">May 2014</a></li>
						<li><a href="http://resources.lifesight.io/blog/2014/04">April 2014</a></li>
						<li><a href="http://resources.lifesight.io/blog/2014/03">March 2014</a></li>
					</ul>
				</div>
			</section>