<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<header class="masthead" id="company-back" style="padding-bottom: 135px;">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Reaching Consumers across Asia</div>
		</div>        
	</div>
</header>

<section id="quotes-dass" class="attr" style="top:535px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<p style="line-height: 32px;font-size: 28px;">We are a consumer intelligence company that drives offline retail purchase for brands, retailers &amp; agencies</p>
			</div>
		</div>
	</div>
</section>

<section class="our-off">
	<div class="section-heading">Our Offices</div>
	<div class="border-heading"></div>
	<ul class="the-country">
		<li>
			<div class="country-name">Singapore</div>
			<div class="overblue">
				<div class="country-add">
					<strong>Ebizu Sdn Bhd</strong>
					Level 8, Ikhlas Point Tower 11,
					Avenue 5, The Horizon, Bangsar South,
					No 8, Jalan Kerinchi,
					59200 Kuala Lumpur
				</div>
			</div>
			<img src="<?php echo $assets_url; ?>img/company/singapore.jpg" />
		</li>
		<li>
			<div class="country-name">Malaysia</div>
			<div class="overblue">
				<div class="country-add">
					<strong>Ebizu Sdn Bhd</strong>
					Level 8, Ikhlas Point Tower 11,
					Avenue 5, The Horizon, Bangsar South,
					No 8, Jalan Kerinchi,
					59200 Kuala Lumpur
				</div>
			</div>
			<img src="<?php echo $assets_url; ?>img/company/malaysia.jpg" />
		</li>
		<li>
			<div class="country-name">Jakarta</div>
			<div class="overblue">
				<div class="country-add">
					<strong>PT. Ebizu Prima Indonesia</strong>
					Level 8, Ikhlas Point Tower 11,
					Avenue 5, The Horizon, Bangsar South,
					No 8, Jalan Kerinchi,
					59200 Kuala Lumpur
				</div>
			</div>
			<img src="<?php echo $assets_url; ?>img/company/jakarta.jpg" />
		</li>
		<li>
			<div class="country-name">Banglore</div>
			<div class="overblue">
				<div class="country-add">
					<strong>Ebizu Sdn Bhd</strong>
					Level 8, Ikhlas Point Tower 11,
					Avenue 5, The Horizon, Bangsar South,
					No 8, Jalan Kerinchi,
					59200 Kuala Lumpur
				</div>
			</div>
			<img src="<?php echo $assets_url; ?>img/company/bangalore.jpg" />
		</li>
	</ul>
</section>