<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<header class="masthead" id="insight">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Understand Your Customers’ Behaviour in the Physical Worlds</div>
			<div class="intro-lead-in">We leverage location and transactional data to identify connected consumers interests, brand affinity, movement behaviour and purchase intent</div>
			<a class="btn btn-xl js-scroll-trigger btn-act" id="playme" onclick="revealVideo('video','youtube')">Play The Video</a>
			<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal">Let's Talk</a>
		</div>        
	</div>
</header>

<div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
	<div class="lightbox-container">  
		<div class="lightbox-content">

			<button onclick="hideVideo('video','youtube')" class="lightbox-close">
				Close | ✕
			</button>
			<div class="video-container">
				<iframe id="youtube" width="960" height="520" src="https://www.youtube.com/embed/rTGom-T9S3E?showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>      

		</div>
	</div>
</div>

<section id="lifesightdata" class="five-element">
	<div class="container">
		<div class="row">
			<div class="row container main-data">
				<div class="part">
					<div class="infographic">
						<div class="title">

						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number">50</div> <span>M+</span>
						</div>
						<div class="content">
							Active User Profiles
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number">300</div> <span>k+</span>
						</div>
						<div class="content">
							Location Tracked
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<span>$</span><div class="number">1</div><span>B</span>
						</div>
						<div class="content">
							Txns Recorded                
						</div>
					</div> 
				</div>
				<div class="part">
					<div class="infographic">
						<div class="title">
							<div class="number slow">100</div><span>%</span>
						</div>
						<div class="content">
							SDK Source               
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Feature -->
<section id="feature" class="two-col">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Features</h2>
				<div class="borderheading"></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="row col-md-6 main-feature">
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/insight/location.png">
						<div class="col-md-8 7ext">
							<div class="title">
								Location
							</div>
							<div class="border"></div>
							<div class="info">
								Our PlaceSense technology offers a variety of brand location insights such as visits, peak times, dwell times, visit frequency and more.
							</div>
						</div>
					</div> 
				</div>
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/insight/consumer.png">
						<div class="col-md-8 text">
							<div class="title">
								Consumer
							</div>
							<div class="border"></div>
							<div class="info">
								We offer anonymous consumer profile insights which enables a view of your location visitors that are on our platform.
							</div>
						</div>
					</div> 
				</div>
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/insight/transaction.png">
						<div class="col-md-8 text">
							<div class="title">
								Transactional
							</div>
							<div class="border"></div>
							<div class="info">
								We collect and process transactional data to provide a sample of spend behaviour across thousands of retailers in Southeast Asia.
							</div>
						</div>
					</div> 
				</div>
				<div class="row part">
					<div class="row insight">
						<img class="col-md-4" src="<?php echo $assets_url; ?>img/insight/product.png">
						<div class="col-md-8 text">
							<div class="title">
								Products
							</div>
							<div class="border"></div>
							<div class="info">
								Analyze the performance of your media campaigns by viewing the change in your location visit data during a certain period. 
							</div>
						</div>
					</div> 
				</div>
				<a data-toggle="modal" data-target="#contactModal"><button class="btn btn-act">
					Get Access To Your Data
				</button></a>
			</div>
			<div class="row col-md-6 screen">
				<img src="<?php echo $assets_url; ?>img/insights-section2.png" width="570" height="664">
			</div>
		</div>
	</div>
</section>

<!-- Industry Solutions -->
<section id="industrysolution" class="fourparts">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Industry Solutions For</h2>
				<div class="borderheading"></div>
			</div>
		</div>
		<div class="row container main-data">
	        <div class="part">
	        	<div class="infographic">
	        		<img src="<?php echo $assets_url; ?>img/industry-solutions/ind-sol-brands.png">
	        		<div class="title">
	        			Brands
	        		</div>
	        	</div>  
	        </div>
	        <div class="part">
	        	<div class="infographic">
	        		<img src="<?php echo $assets_url; ?>img/industry-solutions/ind-sol-medcom.png">
	        		<div class="title">
	        			Media Companies
	        		</div>
	        	</div>  
	        </div>
	        <div class="part">
	        	<div class="infographic">
	        		<img src="<?php echo $assets_url; ?>img/industry-solutions/ind-sol-dsps.png">
	        		<div class="title">
	        			DSPs
	        		</div>
	        	</div> 
	        </div>
	        <div class="part dmps">
	        	<div class="infographic">
	        		<img src="<?php echo $assets_url; ?>img/industry-solutions/ind-sol-dmps.png">
	        		<div class="title">
	        			DMPs
	        		</div>
	        	</div>  
	        </div>
	    </div>
	</div>
</section>
