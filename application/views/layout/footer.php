<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section id="form">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<button class="btn btn-act" data-toggle="modal" data-target="#contactModal">Get Started Now!</button>
				<p>Calling all brands and advertisers who want to get <strong>higher returns on your advertising spend</strong></p>
			</div>
		</div>
	</div>
</section>

<!-- MODAL -->
<div class="modal fade modal-new" id="thxModal" tabindex="-1" role="dialog" aria-labelledby="thxModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<span>Inquiry Sent!</span>
			</div>
			<div class="modal-thx" data-img-success="<?php echo $assets_url; ?>img/inquiry-sent.png" data-img-failed="<?php echo $assets_url; ?>img/inquiry-fail.png">
				<img id="notif-msg-ico" src="<?php echo $assets_url; ?>img/inquiry-sent.png" />
				<div class="thx-1">Thank you for your enquiry.</div>
				<div class="thx-2">Our representative will be in touch as soon as possible.</div>
				<button data-dismiss="modal" aria-label="Close">Ok</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade modal-new" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<span>Contact Us</span>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
			</div>
			<div class="modal-left">
				<form id="lifesight-form-contact" action="<?php echo $app_url; ?>contact/submit" method="POST">
					<ul>
						<li id="first_name" class="duo">
							<label>First Name</label>
							<input type="text" name="first_name" />
						</li>
						<li id="last_name" class="duo">
							<label>Last Name</label>
							<input type="text" name="last_name" />
						</li>
						<li id="email" class="duo">
							<label>Email</label>
							<input type="email" name="email" />
						</li>
						<li id="company" class="duo">
							<label>Company</label>
							<input type="text" name="company" />
						</li>
						<li id="mobile" class="duo">
							<label>Mobile</label>
							<input type="number" name="mobile" />
						</li>
						<li id="website" class="duo">
							<label>Website</label>
							<input type="text" name="website" />
						</li>
						<li id="country">
							<label>Country</label>
							<input type="text" name="country" />
						</li>
						<li id="industry">
							<i class="fa fa-caret-down caret-sel"></i>
							<label>Industry</label>
							<select name="industry">
	                            <option>None</option>
	                            <option>Academic</option>
	                            <option>Advertising/Marketing</option>
	                            <option>Consulting</option>
	                            <option>CPG</option>
	                            <option>Finance</option>
	                            <option>Food &amp; Beverage</option>
	                            <option>Healthcare</option>
	                            <option>Media/Entertainment</option>
	                            <option>Press</option>
	                            <option>Publisher</option>
	                            <option>Research</option>
	                            <option>Retail</option>
	                            <option>Technology</option>
	                            <option>Telecommunications</option>
	                            <option>Travel</option>
	                            <option>Others</option>
							</select>
						</li>
						<li id="lead_source">
							<i class="fa fa-caret-down caret-sel"></i>
							<label>Lead Source</label>
							<select name="lead_source">
								<option>None</option>
								<option>Email</option>
								<option>Social</option>
								<option>Word of Mouth</option>
								<option>Ad</option>
								<option>Search</option>
							</select>
						</li>
						<li id="message">
							<label>Message</label>
							<textarea name="message"></textarea>
						</li>
						<li class="duo">
							<button>Submit</button>
						</li>
					</ul>
				</form>
			</div>
			<div class="modal-right"></div>
		</div>
	</div>
</div>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-4 part">
				<div class="row">
					<div class="col-sm-12 col-xs-12 col-md-12">
						<div class="title">
							QUICK LINKS
						</div>
						<ul>
							<a href="<?php echo $app_url; ?>info/insight"><li>Consumer &amp; Location Insights</li></a>
							<a href="<?php echo $app_url; ?>info/marketing"><li>People Based Marketing</li></a>
							<a href="<?php echo $app_url; ?>info/attribution"><li>Omni-channel Attribution</li></a>
							<a href="<?php echo $app_url; ?>info/daas"><li>Data-as-a-Service</li></a>
							<a href="<?php echo $app_url; ?>resources" target="_blank"><li>Resources</li></a>
							<a href="<?php echo $app_url; ?>company"><li>Company</li></a>
							<a href="<?php echo $app_url; ?>partners"><li>Partners</li></a>
							<a href="http://support.lifesight.io"><li>Support</li></a>
						</ul>  
					</div>
					<div class="col-md-12 socmed">
						<label>Follow Us</label>
						<ul class="social">
							<a href="https://www.facebook.com/ebizudotcom/" target="_blank"><li><img src="<?php echo $assets_url; ?>img/footer/circle-facebook.png"></li></a>
							<a href="https://www.youtube.com/user/ebizudotcom" target="_blank"><li><img src="<?php echo $assets_url; ?>img/footer/circle-youtube.png"></li></a>
							<a href="https://twitter.com/ebizudotcom/" target="_blank"><li><img src="<?php echo $assets_url; ?>img/footer/circle-twitter.png"></li></a>
							<a href="https://www.linkedin.com/company/ebizu" target="_blank"><li><img src="<?php echo $assets_url; ?>img/footer/circle-linkedin.png"></li></a>
							<a href="https://www.instagram.com/ebizudotcom/" target="_blank"><li><img src="<?php echo $assets_url; ?>img/footer/circle-instagram.png"></li></a>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-4 part">
				<div class="col-md-12 accolades">
					<label>Accolades</label>
					<img src="<?php echo $assets_url; ?>img/footer/2015-EBIZU-Award-Logo.gif">
				</div>
				<div class="col-md-12 member">
					<label>A Member of</label>
					<ul class="social">
						<li><img src="<?php echo $assets_url; ?>img/footer/iab-logo.png"></li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 part">
				<div class="col-sm-12 col-xs-12 col-md-12">
					<div class="title">
						THE FINE PRINT
					</div>
					<ul>
						<a href=""><li>Privacy and Security Policy</li></a>
						<a href=""><li>Terms and Conditions</li></a>
					</ul>  
				</div>
			</div>
		</div>
	</div>
</section>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<span class="copyright">Copyright &copy; 2018 - Lifesight Pte. Ltd. All rights reserved.</span>
			</div>
		</div>
	</div>
</footer>
