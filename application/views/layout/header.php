<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
<div class="container">
	<a class="navbar-brand" href="index.html"><img src="<?php echo $assets_url; ?>img/logos/lifesight-logo.svg"></a>
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		Menu
		<i class="fa fa-bars"></i>
	</button>
	<div class="collapse navbar-collapse" id="navbarResponsive">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="<?php echo $app_url; ?>">Home</a>
			</li>
			<li class="nav-item dropdown" style="display: none;">
				<a class="nav-link" data-toggle="dropdown">Products
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo $app_url; ?>info/insight">Consumer &amp; Location Insights</a></li>
						<li><a href="<?php echo $app_url; ?>info/marketing">People Based Marketing</a></li>
						<li><a href="<?php echo $app_url; ?>info/attribution">Omni-channel Attribution</a></li>
						<li><a href="<?php echo $app_url; ?>info/daas">Data-as-a-Service</a></li>
					</ul>
				</li>
				<div class="dropdown">
					<button class="dropbtn">Products</button>
					<div class="dropdown-content">
						<a href="<?php echo $app_url; ?>info/insight">Consumer &amp; Location Insights</a>
						<a href="<?php echo $app_url; ?>info/marketing">People Based Marketing</a>
						<a href="<?php echo $app_url; ?>info/attribution">Omni-channel Attribution</a>
						<a href="<?php echo $app_url; ?>info/daas">Data-as-a-Service</a>
					</div>
				</div>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo $app_url; ?>resources" target="_blank">Resources</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo $app_url; ?>company">Company</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo $app_url; ?>partners">Partners</a>
				</li>
			</ul>
			<ul class="pull-right outside">
				<li class="nav-item">
					<a class="nav-link" href="http://app.lifesight.io/" target="_blank">Login</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="" data-toggle="modal" data-target="#contactModal">Get In Touch</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
