<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo ( ! empty($document['page_title']) ? $document['page_title'] . ' - ' : ''); ?>Lifesight</title>
      
      <link type="img/png" rel="icon" href="<?php echo $assets_url; ?>img/favicon.png">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $assets_url; ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Custom fonts for this template -->
    <link href="<?php echo $assets_url; ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css' />

    <!-- Slick Slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo $assets_url; ?>vendor/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $assets_url; ?>vendor/slick/slick-theme.css" />
    
    <!-- Custom styles for this template -->
    <link href="<?php echo $assets_url; ?>css/agency.css?v=2" rel="stylesheet" />
    <?php
    if ( ! empty($document['css']))
    {
        foreach ($document['css'] as $document_css)
        {
            echo "\n" . '<link href="'.$document_css.'" rel="stylesheet">';
        }
    }
    ?>

	<style>.async-hide { opacity: 0 !important} </style>
	<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
	h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
	(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
	})(window,document.documentElement,'async-hide','dataLayer',4000,
	{'GTM-PM73X2L':true});</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112664893-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-112664893-1');
	</script>
    <script id="_bettercampaign_script" type="text/javascript">
    var __a = 7407; var __ci =1; var __gv =1; 
    (function(){
    var _bc = document.createElement('script');
    _bc.type = 'text/javascript';
    _bc.async = true;
    var _bcWidgetJs = "/bctag.js";
    _bc.src = "https://storage.googleapis.com/bctrackers" +_bcWidgetJs;
    var _sNode = document.getElementById('_bettercampaign_script');
    _sNode.parentNode.insertBefore(_bc, _sNode);
    })();
    </script>
</head>
<body id="page-top" class="<?php echo ( ! empty($document['page_class']) ? $document['page_class'] : 'product'); ?>">
	<?php echo $header; ?>

	<?php echo $content; ?>
	
	<?php echo $footer; ?>
    
    <div id="lifesight-meta" data-assets-url="<?php echo $assets_url; ?>"></div>
	
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $assets_url; ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $assets_url; ?>vendor/popper/popper.min.js"></script>
    <script src="<?php echo $assets_url; ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $assets_url; ?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?php echo $assets_url; ?>js/jqBootstrapValidation.js"></script>
    <script src="<?php echo $assets_url; ?>js/contact_me.js"></script>

    <!-- Slider -->
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $assets_url; ?>vendor/slick/slick.min.js"></script>
    <script src="<?php echo $assets_url; ?>js/readmore.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo $assets_url; ?>js/agency.js"></script>
    <script src="<?php echo $assets_url; ?>js/contact.js"></script>

    <?php
    if ( ! empty($document['js']))
    {
        foreach ($document['js'] as $document_js)
        {
            echo "\n" . '<script src="'.$document_js.'" type="text/javascript"></script>';
        }
    }
    ?>
</body>
</html>