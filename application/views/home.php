<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="overlay"></div>

<video id="video-background" poster="<?php echo $assets_url; ?>img/bg-first2.jpg" autoplay loop muted>
	<source src="<?php echo $assets_url; ?>img/video/LifeSight.mp4">
	Your browser does not support the video tag.
</video>

<header class="masthead" id="main">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Reach &amp; Delight Your Connected Customer in Their Mobile Moment</div>
			<div class="intro-lead-in">We help marketers use online and offline data with our geo-behavioural intelligence AI platform to deliver better ads to the right consumer at the right time.</div>
			<a class="btn btn-xl js-scroll-trigger btn-act" id="playme" onclick="revealVideo('video','youtube')">Play The Video</a>
			<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal">Let's Talk</a>
		</div>      
	</div>
</header>

<div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
	<div class="lightbox-container">  
		<div class="lightbox-content">
			<button onclick="hideVideo('video','youtube')" class="lightbox-close">
				Close | ✕
			</button>
			<div class="video-container">
				<iframe id="youtube" width="960" height="520" src="https://www.youtube.com/embed/rTGom-T9S3E?showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>      

		</div>
	</div>
</div>

<section id="consumer-intellegence">
	<h3 class="section-subheading text-muted">We are a consumer intelligence company that drives offline retail purchase for brands, retailers &amp; agencies</h3>
	<div class="container">
		<div class="row">
			<div class="main-info row col-md-12">
				<div class="col-md-3 part">
					<div class="infographic">
						<div class="title">
							<div class="number">285</div> <span>Million</span>
						</div>
						<div class="content">
							Consumer Profiles Tracked
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="infographic">
						<div class="title">
							<div class="number">15</div> <span>Million</span>
						</div>
						<div class="content">
							Locations Tracked in APAC
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="infographic">
						<div class="title">
							<div class="number">9</div> <span>Billion</span>
						</div>
						<div class="content">
							Events Captured Monthly
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="infographic">
						<div class="title">
							<div class="number slow">21</div> <span>Countries</span>
						</div>
						<div class="content">
							Coverage in APAC
						</div>
					</div> 
				</div>
			</div>
			<div class="main-info img row col-md-12">
				<div class="col-md-3 part">
					<div class="infographic">
						<div class="content">
							<img src="<?php echo $assets_url; ?>img/features/feature-two.png">
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="infographic">
						<div class="content">
							<img src="<?php echo $assets_url; ?>img/insight/location.png">
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="infographic">
						<div class="content">
							<img src="<?php echo $assets_url; ?>img/insight/consumer.png">
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="infographic">
						<div class="content">
							<img src="<?php echo $assets_url; ?>img/insight/transaction.png">
						</div>
					</div> 
				</div>
			</div>

		</div>
	</div>
</section>

<!-- Platform -->
<section id="platform">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading"><img src="<?php echo $assets_url; ?>img/platform/logo-platform.png"> AI Platform</h2>
				<h3 class="section-subheading text-muted">Choose a product for your business needs</h3>
			</div>
		</div>
		<div class="row text-center">
			<div class="row container main-platform">
				<div class="col-md-3 part">
					<div class="insight">
						<img src="<?php echo $assets_url; ?>img/platform/consumer-insight.png">
						<div class="title">
							Consumer &amp; Location Insights
						</div>
						<div class="border"></div>
						<a href=""><button class="btn btn-act">
							Learn More
						</button></a>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">
						<img src="<?php echo $assets_url; ?>img/platform/people-based.png">
						<div class="title">
							People Based Marketing
						</div>
						<div class="border"></div>
						<a href=""><button class="btn btn-act">
							Learn More
						</button></a>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">
						<img src="<?php echo $assets_url; ?>img/platform/omni-channel.png">
						<div class="title">
							Omni-channel Attribution
						</div>
						<div class="border"></div>
						<a href=""><button class="btn btn-act">
							Learn More
						</button></a>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">
						<img src="<?php echo $assets_url; ?>img/platform/DaaS.png">
						<div class="title">
							Data-as-a-Service (DaaS)
						</div>
						<div class="border"></div>
						<a href=""><button class="btn btn-act">
							Learn More
						</button></a>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Quotes -->
<section id="quotes">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h3 class="section-subheading text-muted">Do you know?</h3>
				<h2 class="section-heading">Almost 50% of all mobile advertising spend is wasted due to untimely delivery and lack of context</h2>

			</div>
		</div>
	</div>
</section>

<!-- Consumer Intellegence -->
<section id="brands-slider">
	<div class="container">
		<div class="row text-center">
			<h2>
				Brands That Love Us
			</h2>
			<div class="autoplay">
				<div><img src="<?php echo $assets_url; ?>img/brands/Caltex.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/coffeeday.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/courts.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/digi.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/FWD-Life-Insurance-Logo.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/gopro.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/housejoy.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/hsbc.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/kotak.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/mitsubishi.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/sakae_sushi.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/shilla.png"></div>
	            <div><img src="<?php echo $assets_url; ?>img/brands/subway.png"></div>
			</div>
		</div>
	</div>
</section>

<!-- Place Sense -->
<section id="place-sense">
	<div class="container">
		<div class="row tex-center">
			<div class="col-md-6 text">
				<img src="<?php echo $assets_url; ?>img/PlaceSense.png">
				<div class="title">
					Micro Location Insights &amp; Intelligence
				</div>
				<div class="border"></div>
				<span>Introducing the most deterministic micro location intelligence platform in Asia that collects GPS, WiFi &amp; Beacons data via a network of premium app publishers to accurately drive people movement insights use cases.</span>
			</div>
			<div class="col-md-6 display">
				<img height="540" src="<?php echo $assets_url; ?>img/video/Maps_Web_Blue_NV.gif">
			</div>
		</div>
	</div>
</section>

<section id="identification">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">We know your customers, anonymously</h2>
				<h3 class="section-subheading text-muted">We track and profile millions of customers demographics, behaviours, interests and preferences anonymously to enable advertisers to target consumers with highly personalized experiences</h3>
			</div>
		</div>
		<div class="row text-center">
			<div class="row container main-customer">
				<div class="col-md-3 part">
					<div class="insight">
						<div class="box">
							<img class="phopic" src="<?php echo $assets_url; ?>img/customers/John.png">
							<i><img src="<?php echo $assets_url; ?>img/customers/icons/icon1.png"></i>
							<div class="title">
								3449532355
							</div>
							<div class="profile">
								Male, 36, Kuala Lumpur <span>Consultant, High Spend</span>
							</div>
							<div class="define">
								Coffee Lover, Fitness Freak, Movie Buff, Traveller
							</div>
							<div class="seen">
								<ul>
									<li>Seen at coffee outlets</li>
									<li>Seen at malls often</li>
									<li>Seen at baby shops and gyms</li>
									<li>See ads 3 times today</li>
									<li>Apps: CIMB Bank, Expedia, Booking.com, Bloomberg</li>
								</ul>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">
						<div class="box">
							<img class="phopic" src="<?php echo $assets_url; ?>img/customers/Adam.png">
							<i><img src="<?php echo $assets_url; ?>img/customers/icons/icon2.png"></i> 
							<div class="title">
								3449532355
							</div>
							<div class="profile">
								Female, 20, Jakarta <span>Student, Low Spend</span>
							</div>
							<div class="define">
								Music Lover, Movie/TV Buff, Gamer, Online Shopper
							</div>
							<div class="seen">
								<ul>
									<li>Seen at restaurants and cafes</li>
									<li>Seen at malls weekly</li>
									<li>Seen on online shopping sites</li>
									<li>See ads 10 times today</li>
									<li>Apps: Lazada, Zalora, Spotify, Amazon, Netflix</li>
								</ul>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">
						<div class="box">
							<img class="phopic" src="<?php echo $assets_url; ?>img/customers/Kevi.png">
							<i><img src="<?php echo $assets_url; ?>img/customers/icons/icon3.png"></i>
							<div class="title">
								3449532355
							</div>
							<div class="profile">
								Male, 48, Singapore <span>Banker, High Spend</span>
							</div>
							<div class="define">
								Traveler, Golfer, Fine Dining, Live Entertainment
							</div>
							<div class="seen">
								<ul>
									<li>Seen at restaurants and bars</li>
									<li>Seen at golf courses</li>
									<li>Seen at airports</li>
									<li>See ads 6 times today</li>
									<li>Seen at restaurants and bars</li>
									<li>Seen at golf courses</li>
									<li>Seen at airports</li>
									<li>See ads 6 times today</li>
									<li>Apps: Bloomberg, CNN, GolfBuddy, Singapore Airlines</li>
								</ul>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-3 part">
					<div class="insight">
						<div class="box">
							<img class="phopic" src="<?php echo $assets_url; ?>img/customers/Vicky.png">
							<i><img src="<?php echo $assets_url; ?>img/customers/icons/icon4.png"></i>
							<div class="title">
								3449532355
							</div>
							<div class="profile">
								Female, 32, Singapore <span>Marketer, High Spend</span>
							</div>
							<div class="define">
								Shopaholic, Coffee Lover, Sweeth tooth, Mom
							</div>
							<div class="seen">
								<ul>
									<li>Seen at hypermarket and mall</li>
									<li>Seen at apparel &amp; shoe shops</li>
									<li>Seen at kids shops</li>
									<li>Seen ads 15 times today</li>
									<li>Apps: MomsCare, Spotify, Netflix, CandyCrush</li>
								</ul>
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<section id="casestudies">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Case Studies</h2>
				<h3 class="section-subheading text-muted">Proven case studies that is poised to disrupt the advertising world like never before. We can now answer the age old question retail brands have.. Is my advertising spend driving sales in my retail store?</h3>
			</div>
		</div>
		<div class="row text-center">
			<div class="row container main-retail" style="margin:0px;">
				<div class="col-md-4 part">
					<div class="insight">
						<img class="merchantpic" src="<?php echo $assets_url; ?>img/case_study_1.png">
						<div class="content">
							The brand ran a location-based campaign with redeemable discount vouchers via mobile app advertising platform with the objective to boost sales. During the 75 days, the brand enjoyed a significant increase in sales and revenue.
						</div>
		                <a href="<?php echo $assets_url; ?>doc/case-study-template-3.pdf" target="_blank"><div class="view" style="background: #82C211;border-color:#82C211;">
		                  View Case Study
		                </div></a>
					</div> 
				</div>
				<div class="col-md-4 part">
					<div class="insight">
						<img class="merchantpic" src="<?php echo $assets_url; ?>img/case_study_2.png">
						<div class="content">
							The brand ran a location based campaign with a redeemable discount voucher via a mobile app advertising channel to boost sales. During the 3 months campaign, the brand saw significant results and increased customer loyalty.
						</div>
		                <a href="<?php echo $assets_url; ?>doc/case-study-template-1.pdf" target="_blank"><div class="view" style="background: #82C211;border-color:#82C211;">
		                  View Case Study
		                </div></a>
					</div> 
				</div>
				<div class="col-md-4 part">
					<div class="insight">
						<img class="merchantpic" src="<?php echo $assets_url; ?>img/case_study_3.png">
						<div class="content">
							The strategy implemented was digital campaigns with redeemable in-app e-vouchers. The campaign was planned and commenced for three weeks with great success in reference to the sales and the customer retention achieved.
						</div>
		                <a href="<?php echo $assets_url; ?>doc/case-study-template-2.pdf" target="_blank"><div class="view" style="background: #82C211;border-color:#82C211;">
		                  View Case Study
		                </div></a>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>