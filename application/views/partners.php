<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Header -->
<header class="masthead" id="partners" style="padding-bottom: 135px;">
	<div class="container">
		<div class="intro-text">
			<div class="intro-heading">Take Your Business to the Next Level With Our Partner Program</div>
			<div class="intro-lead-in">We leverage location and transactional data to identify connected consumers interests, brand affinity, movement behaviour and purchase intent</div>
			<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal" style="width: 400px;">JOIN PARTNER PROGRAM</a>
		</div>        
	</div>
</header>

<div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
	<div class="lightbox-container">  
		<div class="lightbox-content">
			<button onclick="hideVideo('video','youtube')" class="lightbox-close">
				Close | ✕
			</button>
			<div class="video-container">
				<iframe id="youtube" width="960" height="520" src="https://www.youtube.com/embed/rTGom-T9S3E?showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>      

		</div>
	</div>
</div>

<!-- Quotes DaaS -->
<section id="quotes-dass" class="attr" style="top:500px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<p style="line-height: 1.75;font-size: 28px;">Are you a <br>Media Company, Publisher, DSP or App Developer?</p>

			</div>
		</div>
	</div>
</section>

<!-- How It Works -->
<section id="usecase" class="trio how partners">
	<div class="row atr">
		<div class="text-center col-md-12">
			<h2 class="section-heading">Partner Programs</h2>
			<div class="borderheading"></div>
			<p>Our partner program is the most important part of enhancing the value of our targeting, data and measurement offering. We have designed unique proposition for each partner type that will help you improve your monetization capability and serve your customer better.</p>
		</div>
		<div class="text-center col-md-12">
			<div class="row container main-feature">
				<div class="col-md-4 part">
					<div class="insight">
						<img class="row" src="<?php echo $assets_url; ?>img/partners/certified.png" height="161">
						<div class="text">
							<div class="title">
								PlaceSense Certified<br> Publisher
							</div>
							<div class="border"></div>
							<div class="info">
								This program is designed for media companies and premium publishers who want to utilize our attribution solution as a 3rd party measurement tool for their digital ads.
							</div>
							<div class="click">
								<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal">BECOME A PARTNER</a>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-4 part">
					<div class="insight">
						<img class="row" src="<?php echo $assets_url; ?>img/partners/networks.png" height="161">
						<div class="text">
							<div class="title">
								DSP &amp;<br> Ad Networks
							</div>
							<div class="border"></div>
							<div class="info">
								This program is designed for DSPs and Ad Networks who want to offer offline attribution to their digital ads customers so they can enhance their attribution offering beyond clicks. 
							</div>
							<div class="click">
								<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal">BECOME A PARTNER</a>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-4 part">
					<div class="insight">
						<img class="row" src="<?php echo $assets_url; ?>img/partners/mobile.png" height="161">
						<div class="text">
							<div class="title">
								Mobile App<br> Developers
							</div>
							<div class="border"></div>
							<div class="info">This program is designed for media companies and premium publishers who want to utilize our attribution solution as a 3rd party measurement tool for their digital ads.
							</div>
							<div class="click">
								<a class="btn btn-xl js-scroll-trigger btn-passive" data-toggle="modal" data-target="#contactModal">BECOME A PARTNER</a>
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Consumer Intellegence -->
<section id="brands-slider" style="background: #F8F8F8;">
	<div class="container">
		<div class="row text-center">
			<h2>
				Our Trusted Partners
			</h2>
			<div class="autoplay">
				<div><img src="<?php echo $assets_url; ?>img/partnership/adplus.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/apd.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/blackstone.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/cda.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/consider-digital.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/emc-group.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/entropia.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/fmg.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/groupm.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/ipg.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/lion-n-lion.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/moving-walls.png"></div>
				<div><img src="<?php echo $assets_url; ?>img/partnership/omd.png"></div>
			</div>
		</div>
	</div>
</section>